# Desafio Técnico Front-End Mobile Conexa

> O desafio foi feito utilizando VueJs, Bootstrap e Axios

## Configurações iniciais

#### - Abra a pasta raiz do projeto atravéz do console de comando e execute o seguinte comando:
$ npm install

#### - Após instalar todas as dependências necessárias rode o projeto com o seguinte comando:
$ npm run serve

* O URL para visualizar o projeto é http://localhost:8080, default do Vuejs.

