import http from '../http-common'

class LoginService {
    login(data) {
        return http.post('/login', data)
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('user')
    }

}

export default new LoginService()






