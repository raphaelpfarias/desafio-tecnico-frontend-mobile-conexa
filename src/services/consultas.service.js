import http from '../http-common'

class ConsultasService {
    getAllConsultas() {
        return http.get('/consultas')
    }

    getConsultaById(id) {
        return http.get(`/consulta/${id}`)
    }

    createConsulta(data) {
        return http.post('/consulta', data)
    }


}

export default new ConsultasService()






