import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import App from './App.vue'
import router from './router'
import axios from 'axios'

axios.interceptors.request.use(config => {
  let token = JSON.parse(localStorage.getItem("user")).token;
  config.headers = Object.assign({
    "Content-type": 'application/json',
    Authorization: `Bearer ${token}`
  }, config.headers)
  return config
},
  error => Promise.reject(error)
)

Vue.use(BootstrapVue)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
