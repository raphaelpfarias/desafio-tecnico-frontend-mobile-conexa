import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginPage from '../views/LoginPage'
import ConsultasPage from '../views/ConsultasPage'
import NovaConsultaPage from '../views/NovaConsultaPage'
import ConsultaDetails from '../views/ConsultaDetails'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: {
      name: "login"
    }
  },
  {
    path: '/consultas',
    name: 'consultas',
    component: ConsultasPage
  },
  {
    path: '/nova-consulta',
    name: 'nova-consulta',
    component: NovaConsultaPage
  },
  {
    path: '/login',
    name: 'login',
    component: LoginPage
  },
  {
    path: '/consulta/:id',
    name: 'consulta-detail',
    component: ConsultaDetails
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/login'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');
  if (authRequired && !loggedIn) {
    return next({
      path: '/login',
      query: { returnUrl: to.path }
    });
  }

  next();
})

export default router
