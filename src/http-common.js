import axios from 'axios'

const instance = axios.create({
  baseURL: 'http://desafio.conexasaude.com.br/api',
  headers: {
    'Content-type': 'application/json'
  }
})

instance.interceptors.request.use(function (config) {
  const user = JSON.parse(localStorage.getItem("user"));
  if (user != null) {
    config.headers.Authorization = `Bearer ${user.token}`;
  }
  return config;
}, function (err) {
  return Promise.reject(err);
});

export default instance
